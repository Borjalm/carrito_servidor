<!DOCTYPE html>
<html lang="es">

<head>
    <link rel=StyleSheet href="confirmacion.css" TYPE="text/css" />
</head>

<body>
<div id="contenido" style="width:399px; margin-left: 5%; ">
    <?php
    session_start();
    if (isset($_SESSION["usuario"]) && isset($_SESSION["roles"])) {
        echo "<p id='centrados';>ARTICULOS</p><hr style='border-color:white;'>";
    }
    ?>
    <table border="1px solid" style="text-align:center;" >
        <tr style="background-color:#f7dc6f;height:30px;width:80px; margin-left:auto;">
            <th>Identificación</th>
            <th>Descripción</th>
            <th>Precio</th>
            <th>Caracteristicas</th>
        </tr>
        <tr>

        <?php
        $conexion = mysqli_connect("localhost", "consultor", "consultor", "ventas");
        $sql = "SELECT idarticulo,descripcion,precio,caracteristicas FROM articulos";
        $resultado = mysqli_query($conexion, $sql);
        if (mysqli_num_rows($resultado) > 0) {
            while ($registro = mysqli_fetch_row($resultado)) {
                $idart = $registro[0];
                $descripcion = $registro[1];
                $precio = $registro[2];
                $caracteristicas = $registro[3];
                echo "<tr><td style='background-color:white;'>" . $idart . "</td>";
                echo "<td style='background-color:white;'>" . $descripcion . "</td>";
                echo "<td style='background-color:white;'>" . $precio . "</td>";
                echo "<td style='background-color:white;'>" . $caracteristicas . "</td>";
            }
        }
        ?>
        </tr>
    </table>
    <form action="" method="post">
    <input name="atras" type="Submit" value="Atrás" class="botones" style="float:right;margin-right:4px;">
        <input name="Cerrar" type="Submit" value="Cerrar" class="botones" style="float:right;margin-right:4px;">
    </form>
    <hr style='border-color:white;'>
    <?php
    if (isset($_POST["Cerrar"])) {
        session_destroy();
        mysqli_close($conexion);
        header("Location:index.php");
    }
    if (isset($_POST["atras"])) {
     
        header("Location:registroarticulos.php");
    }
    ?>
 
</div>

</body>

</html>