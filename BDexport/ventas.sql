-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-01-2020 a las 14:08:44
-- Versión del servidor: 10.4.8-MariaDB
-- Versión de PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ventas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulos`
--

CREATE TABLE `articulos` (
  `idarticulo` int(11) NOT NULL,
  `descripcion` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `precio` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `caracteristicas` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `dire` varchar(535) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `articulos`
--

INSERT INTO `articulos` (`idarticulo`, `descripcion`, `precio`, `caracteristicas`, `dire`) VALUES
(2, 'aprende php sin dolor', '21', 'Libro', 'img/img1.jpg'),
(3, 'el libro negro del programador', '30', 'libro', 'img/img2.jpg'),
(4, 'no me hagas pensar', '13', 'libro', 'img/img3.jpg'),
(5, 'java para novatos', '34', 'libro', 'img/img5.jpg'),
(6, 'aprende sql', '15', 'libro', 'img/img4.jpg'),
(7, 'el manual debian', '17', 'libro', 'img/img6.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras`
--

CREATE TABLE `compras` (
  `idusu` int(11) NOT NULL,
  `idart` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `cantidad` int(20) NOT NULL,
  `precio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `compras`
--

INSERT INTO `compras` (`idusu`, `idart`, `fecha`, `cantidad`, `precio`) VALUES
(18, 2, '2020-01-27 13:01:10', 2, 21),
(18, 2, '2020-01-27 13:01:15', 2, 21),
(18, 3, '2020-01-27 13:01:10', 1, 30),
(18, 3, '2020-01-27 13:01:15', 1, 30),
(18, 4, '2020-01-27 13:01:10', 1, 13),
(18, 4, '2020-01-27 13:01:15', 1, 13),
(18, 5, '2020-01-27 13:01:10', 2, 34),
(18, 5, '2020-01-27 13:01:15', 2, 34),
(18, 6, '2020-01-27 13:01:10', 1, 15),
(18, 6, '2020-01-27 13:01:15', 1, 15),
(18, 7, '2020-01-27 13:01:10', 1, 17),
(18, 7, '2020-01-27 13:01:15', 1, 17),
(20, 4, '2020-01-27 13:01:35', 1, 13),
(20, 4, '2020-01-27 13:01:39', 1, 13),
(27, 6, '2020-01-27 13:01:09', 3, 15),
(27, 6, '2020-01-27 13:01:23', 3, 15);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `idusuario` int(11) NOT NULL,
  `usuario` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `password` longtext COLLATE utf8_spanish_ci NOT NULL,
  `rol` varchar(15) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idusuario`, `usuario`, `password`, `rol`) VALUES
(18, 'pepe', '14ae2b0930cf6ca370da58c4a9200758c0fe503167a66eed66dd1da15a026f020b458a43ddd94dfbd2aa35955a04977c83e8ce98019a6e554abad0b547182a36', 'consultor'),
(19, 'maria', '0161ee5a292943debfa5d2eabdaaf483903851d350c914b9534f625dc309aa65f5586f4c892cd2451365c4f6cb1919d7b33e20403ec98d15cdb4661d741417bb', 'administrador'),
(20, 'jorge', 'd77b79641611f73c2dbbe528c21bdcc552ea348f5d8e50140332cce7b7927f7130daf00234621a4c366eb2d6f0df39aa2589c0024802b721bafe742b43c381ca', 'consultor'),
(27, 'marcos', 'e33fcf9850e4d5da41588122c00ce78f2fa74e11ce90f43d9be51f682ef1adab097da32902f432d81775bc72793615dddc43415d485151fdb12a6aa6ae1e92e1', 'consultor');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `articulos`
--
ALTER TABLE `articulos`
  ADD PRIMARY KEY (`idarticulo`);

--
-- Indices de la tabla `compras`
--
ALTER TABLE `compras`
  ADD PRIMARY KEY (`idusu`,`idart`,`fecha`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idusuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `articulos`
--
ALTER TABLE `articulos`
  MODIFY `idarticulo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idusuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


# Privilegios para `administra`@`localhost`

GRANT USAGE ON *.* TO 'administra'@'localhost' IDENTIFIED BY PASSWORD '*18B4B47F64C5D036803C0F87B169324D7C0D05E9';

GRANT ALL PRIVILEGES ON `ventas`.* TO 'administra'@'localhost' WITH GRANT OPTION;


# Privilegios para `consultor`@`localhost`

GRANT USAGE ON *.* TO 'consultor'@'localhost' IDENTIFIED BY PASSWORD '*DA56441D8553DC1BD5A7B37C676111770F38217E';

GRANT SELECT ON `ventas`.* TO 'consultor'@'localhost';

GRANT SELECT, INSERT ON `ventas`.`compras` TO 'consultor'@'localhost';

GRANT SELECT ON `ventas`.`articulos` TO 'consultor'@'localhost';

GRANT SELECT (idusuario) ON `ventas`.`usuarios` TO 'consultor'@'localhost';