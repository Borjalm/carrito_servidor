<!DOCTYPE HTML>
<html lang='es'>

<head>
    <title>Productos</title>
    <meta charset='UTF-8' />
    <link rel=StyleSheet href="productos.css" type="text/css" />
</head>
<?php
session_start();
$totalpago = 0;
$resultado = 0;
if (isset($_POST["Ingresar"])) {
    $_SESSION["nombre"] = $_POST["nombre"];
}
?>

<body>
    <script>
        document.onkeydown = function(e) {
            tecla = (document.all) ? e.keyCode : e.which;
            if (tecla == 116) {
                return false;
            }
        }
    </script>
    <div id="contenedor">
        <img src="Logotipo.png" id="logo"/>
        <form action="" method="POST">
            <div id="fecha">
                <input type="submit" name="pedido" value="Consultar" class="botones" style="border-left:4.0px solid #f7dc6f ; width:107px;margin-right:0px;" />
                <input type="submit" name="cierre" value="Cerrar sesión" class="botones" style="width:109px;" />
            </div>
        </form>
        <div id="misarticulos">
            <h2>
                <p>Bienvenido a Comic Sans Company, <div id="sub"><?php echo $_SESSION["usuario"]; ?> </div>
                </p>
            </h2>
            <?php
            $conexion = mysqli_connect("localhost", "consultor", "consultor", "ventas");
            $sql3 = "SELECT idarticulo,descripcion,precio,caracteristicas,dire FROM articulos";
            $anadido = 0;
            $resultado = mysqli_query($conexion, $sql3);
            echo '<form action="" method="POST">';
            if (mysqli_num_rows($resultado) > 0) {
                echo "<div id='margen'>";
                while ($registro = mysqli_fetch_row($resultado)) {
                    $producto = $registro[1];
                    $precio = $registro[2];
                    $imagen = $registro[4];
                    $id = $registro[0];
                    echo "</br>Título:" . $producto . "</br></br>";
                    echo "<img src='$imagen' class='tamimg'/></br>";
                    echo "Precio:" . $precio . "€</br>";
                    echo "<button type='submit' style='width:80px' value='$id' class='botones' name='anadir'>Añadir</button></br></br>   ";
                }
                echo "</div>";
            }
            echo "</form>";
            ?>
        </div>
        <div id="confirmacion">
            <?php
            echo "<form action='' method='post'>";
            $total = 0;
            if (isset($_POST['anadir'])) {
                $_SESSION['idprod'] = $_POST["anadir"];
                // header('location:sumaresta.php');  
                $_SESSION['compra'] = true;
                $_SESSION["carrito"][$_SESSION['idprod']][0]++;
                foreach ($_SESSION["carrito"] as $dato) {
                    if ($dato[0] > 0) {
                        $total += $dato[0] * $dato[3];
                        echo $dato[0] . " x " . $dato[2] . "<button type='submit' value='$dato[1]'class='botonesquit' name='quitar'>-</button> <br/><br/>";
                    }
                }
            }
            if (isset($_POST["quitar"])) {
                $idprod = $_POST["quitar"];
                if ($_SESSION["carrito"][$idprod][0] > 0) {
                    $_SESSION["carrito"][$idprod][0]--;
                }
                foreach ($_SESSION["carrito"] as $dato) {
                    if ($dato[0] > 0) {
                        $total += $dato[0] * $dato[3];
                        echo $dato[0] . " x " . $dato[2] . "<button type='submit' value='$dato[1]'class='botonesquit' name='quitar'>-</button> <br/><br/>";
                    }
                }
            }
            if ($total > 0) {
                echo "<div id='raya'sd></div><b>";
                echo "<div id='resultadocarro'> <img src='carrito.png' id='carrito'/>" .  $total . "€  </div>";
                $_SESSION['total'] = $total;
                echo "</b>";
                echo "<div id='botonconfir'><input type='submit' name='confirmar' value='confirmar' class='botonescon'/></div>";
            };
            echo "</form>";
            if (isset($_POST["pedido"])) {
                echo "<form action='' method='POST'> ";
                echo "<p>FECHAS</p><hr>";
                echo "<p>Desde<input type='date' name='fechaini' style='margin-left:10px;'/></br></br>";
                echo "<p>Hasta<input type='date' name='fechafin'  style='margin-left:15px;'/></p>";
                echo "<input type='submit' name='comprobar' value='comprobar' style='width:145px;margin-left:57px;' class='botones'/>";
                echo "</br></br><hr><input type='submit' name='todo'value='ver todo' style='width:145px;margin-left:57px;margin-top:10%;'class='botones'/>";
                echo "</form>";
            }
            if (isset($_POST['comprobar'])) {
                $fechaini = $_POST["fechaini"];
                $fechafin = $_POST["fechafin"];
                if ($fechaini < $fechafin) {
                    $idusu = $_SESSION["idusu"];
                    $conexion = mysqli_connect("localhost", "consultor", "consultor", "ventas");
                    $sql = "SELECT compras.idusu,compras.idart,compras.fecha,compras.cantidad,compras.precio,articulos.descripcion,articulos.idarticulo 
                FROM compras INNER JOIN articulos ON articulos.idarticulo=compras.idart WHERE compras.idusu='$idusu' AND compras.fecha BETWEEN '$fechaini' AND '$fechafin';";

                    $resultado = mysqli_query($conexion, $sql);
                    echo "<div id='latabla'>";
                    echo "<table border='1px solid #f7dc6f' style='text-align:center;'>";
                    echo "<tr>";
                    echo "<th style='background-color:#f7dc6f'>FECHA</th>";
                    echo "<th style='background-color:#f7dc6f'>PRODUCTO</th>";
                    echo "<th style='background-color:#f7dc6f'>PRECIO</th>";
                    echo "<th style='background-color:#f7dc6f'>CANTIDAD</th>";
                    echo "</tr>";
                    if (mysqli_num_rows($resultado) > 0) {
                        while ($registro = mysqli_fetch_row($resultado)) {
                            $fecha = $registro[2];
                            $producto = $registro[5];
                            $precio = $registro[4];
                            $cantidad = $registro[3];
                            echo "<tr>";
                            echo "<td>" . date("d/m/Y", strtotime($fecha)) . "</td>";
                            echo "<td>" . $producto . "</td>";
                            echo "<td>" . $precio . "€</td>";
                            echo "<td>" . $cantidad . "</td>";
                            echo "</tr>";
                        }
                    }
                } else {
                    echo "Escribe fechas válidas";
                }
            }




            if (isset($_POST['todo'])) {
                $idusu = $_SESSION["idusu"];
                $sql20 = "SELECT compras.idusu,compras.idart,compras.fecha,compras.cantidad,compras.precio,articulos.descripcion,articulos.idarticulo 
                FROM compras INNER JOIN articulos ON articulos.idarticulo=compras.idart WHERE compras.idusu='$idusu'";
                $resultado = mysqli_query($conexion, $sql20);
                echo "<div id='latabla'>";
                echo "<table border='1px solid #f7dc6f' style='text-align:center;'>";
                echo "<tr>";
                echo "<th style='background-color:#f7dc6f'>FECHA</th>";
                echo "<th style='background-color:#f7dc6f'>PRODUCTO</th>";
                echo "<th style='background-color:#f7dc6f'>PRECIO</th>";
                echo "<th style='background-color:#f7dc6f'>CANTIDAD</th>";
                echo "</tr>";
                if (mysqli_num_rows($resultado) > 0) {
                    while ($registro = mysqli_fetch_row($resultado)) {
                        $fecha = $registro[2];
                        $producto = $registro[5];
                        $precio = $registro[4];
                        $cantidad = $registro[3];
                        echo "<tr>";
                        echo "<td>" . date("d/m/Y", strtotime($fecha)) . "</td>";
                        echo "<td>" . $producto . "</td>";
                        echo "<td>" . $precio . "€</td>";
                        echo "<td>" . $cantidad . "</td>";
                        echo "</tr>";
                    }
                }
                echo "</table>";
                echo "</div>";
            }
            if (isset($_POST["cierre"])) {
                session_destroy();
                mysqli_close($conexion);
                header("Location:index.php");
            }
            if (isset($_POST['confirmar'])) {
                header('location:confirmacion.php');
            }
            ?>
        </div>
    </div>
    <script>
    </script>
</body>

</html>